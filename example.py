import os
from prairiepy import PrairieLearn

pl = PrairieLearn(os.getenv('PL_API_KEY'))

course_options = {
    'course_instance_id': 123,
}

# api wrappers return a requests.Response object
print(pl.get_gradebook(course_options).json())
