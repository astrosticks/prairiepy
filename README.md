# prairielearn.py 

prairielearn.py aims to be a small Python library for accessing PrairieLearn API endpoints, as well as offering quick methods for scraping data.

## Credits

Big thanks to [jbrightuniverse](https://github.com/jbrightuniverse) for forking [cs221bot](https://github.com/Person314159/cs221bot) and creating [cs213bot](https://github.com/jbrightuniverse/cs213bot), which contained the initial methods that prairielearn.py is based off of.

